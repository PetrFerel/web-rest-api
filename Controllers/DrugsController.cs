﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PharmaceuticalCompany.Data;
using PharmaceuticalCompany.Model;

namespace PharmaceuticalCompany.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DrugsController : ControllerBase
    {
        private readonly AppDbContext _drugs;

        public DrugsController(AppDbContext drugs)
        {
            _drugs = drugs;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Drugs>>> getDrugs()
        {
            try
            {
                return await _drugs.Drugses.ToListAsync();
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<Drugs>>> GetDrugsId(int id)
        {
            try
            {
                var drug = _drugs.Drugses.FindAsync(id);
                if (drug == null)
                {
                    return NotFound($"drugs is not found witch {id}");
                }
                return Ok(drug);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        public async Task<ActionResult<IEnumerable<Drugs>>> PostDrugs(Drugs drugs)
        {
            try
            {
                await _drugs.AddAsync(drugs);
                _drugs.SaveChanges();
                return Ok(drugs);
            }
            catch (Exception ex)
            {

                return BadRequest($"Drugs is added in DB {ex.Message}");
            }
        }
        [HttpPut]
        public async Task<ActionResult<IEnumerable<Drugs>>> PutDrugs(Drugs drugs)
        {

            try
            {
                if (drugs == null || drugs.DrugsId == 0)
                {
                    if (drugs == null)
                    {
                        BadRequest("Drugs is not found witch ID");
                    }
                    else if (drugs.DrugsId == 0)
                    {
                        return BadRequest($"drugs is not found witch {drugs.DrugsId} ID");
                    }


                }
                var   dr  = await _drugs.Drugses.FindAsync(drugs.DrugsId);

                if (dr == null)
                {
                    return NotFound($"Drugs ID is not found {drugs.DrugsId}");
                }
                dr.DrugsId = drugs.DrugsId;
                dr.Name_D = drugs.Name_D;
                dr.Date_VIP_D = drugs.Date_VIP_D;
                dr.Srok_Godn = drugs.Srok_Godn;
                _drugs.SaveChanges();
                return Ok("Drugs datails update.");
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
          
        }
        [HttpDelete]
        public async Task<ActionResult<Drugs>> DelDrugs(int id)
        {
            try
            {
                var dr = await _drugs.Drugses.FindAsync(id);
                if (dr == null)
                {
                    return NotFound($"Drugs is not found witch {id} ID");

                }
                _drugs.Drugses.Remove(dr);
                await _drugs.SaveChangesAsync();
                return Ok($"Drugs delite {dr}");
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

    }
}

