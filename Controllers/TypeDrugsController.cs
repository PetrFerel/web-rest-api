﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PharmaceuticalCompany.Data;
using PharmaceuticalCompany.Model;

namespace PharmaceuticalCompany.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TypeDrugsController : ControllerBase
    {
        private readonly AppDbContext _typeDrugs;

        public TypeDrugsController(AppDbContext typeDrugs)
        {
            _typeDrugs = typeDrugs;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TypeDrugs>>>GetTypeDr()
        {
            try
            {
                return await _typeDrugs.Type_Drugses.ToListAsync();
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            
        }
        [HttpGet("id")]

        public  async   Task<ActionResult<IEnumerable<TypeDrugs>>>GetTypeID(int id)
        {
            try
            {
                var rez = _typeDrugs.Type_Drugses.FindAsync(id);
                if (rez == null)
                {
                    return NotFound($"typeDrugs is not found witch {id} in data base");
                }
                return Ok(rez);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        [HttpPost]  
        public  async   Task<ActionResult<IEnumerable<TypeDrugs>>>GetTypePost(TypeDrugs typeDrugs)
        {
            try
            {
                await _typeDrugs.Type_Drugses.AddAsync(typeDrugs);
                _typeDrugs.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {

                return BadRequest($"Data Type_Druges added in DataBase {ex.Message}");
            }  
        }
        [HttpPut]  
        public async Task<ActionResult<IEnumerable<TypeDrugs>>>PutTypeDrugs(TypeDrugs typeDrugs)
        {
            if (typeDrugs==null || typeDrugs.TypeDrugsId==0)
            {
                if (typeDrugs==null)
                {
                    return NotFound($"TypeDruges is not found witch  ID");
                }else if (typeDrugs.TypeDrugsId==0)
                {
                    return BadRequest($" TypeDruges not found {typeDrugs.TypeDrugsId} ID");

                }
            }

            try
            {
                var rez = await _typeDrugs.Type_Drugses.FindAsync(typeDrugs.TypeDrugsId);
                rez.Type_D = typeDrugs.Type_D;
                rez.Name_D = typeDrugs.Name_D;
                _typeDrugs.SaveChanges();
                return Ok("Update Drugs");
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        public async Task<ActionResult<IEnumerable<TypeDrugs>>> DeliteTypeDrugs(int id)
        {
            try
            {
                var delrez = _typeDrugs.Type_Drugses.FindAsync(id);
                if (delrez == null)
                {
                    NotFound($"TypeDrugs is not found witch {id} ID");
                }
                _typeDrugs.Remove(delrez);
                await _typeDrugs.SaveChangesAsync();
                return Ok($"TypeDrugs Delite {id}");
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
          
    }
}
