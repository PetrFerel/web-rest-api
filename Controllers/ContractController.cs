﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PharmaceuticalCompany.Data;
using Contract = PharmaceuticalCompany.Model.Contract;

namespace PharmaceuticalCompany.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContractController : ControllerBase
    {
        private readonly AppDbContext _contract;

        public ContractController(AppDbContext Contract)
        {
            _contract = Contract;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Model.Contract>>> GetContract()
        {
            try
            {
                return await _contract.Contracts.ToListAsync();
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }

        }

        [HttpGet("id")]
        public async Task<ActionResult<IEnumerable<Contract>>> GetId(int id)
        {
            try
            {
                var result = _contract.Contracts.FindAsync(id);
                if (result == null)
                {
                    return NotFound($"Object is not found on Database {id} ID");
                }
                return Ok(result);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        public async Task<ActionResult<IEnumerable<Contract>>> GetPost(Contract contract)
        {
            try
            {
                await _contract.Contracts.AddAsync(contract);
                _contract.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        [HttpPut]
        public async Task<ActionResult<IEnumerable<Contract>>> Put(Contract contract)
        {
            try
            {
                if (contract == null || contract.ContractId == 0)
                {
                    if (contract == null)
                    {
                        return NotFound($"Contract is not found witch  ID");
                    }
                    else if (contract.ContractId == 0)
                    {
                        return BadRequest($"Contact not found {contract.ContractId} ID (sorry)");
                    }

                }
                var result = await _contract.Contracts.FindAsync(contract.ContractId);
                // To Do 
                result.ID_C = contract.ContractId;
                result.ID_D = contract.ID_D;
                result.ID_E = contract.ID_E;    
                result.ID_K = contract.ID_K;    
                result.Data_C = contract.Data_C;    
                result.Kolvo_D = contract.Kolvo_D;  
                result.Cena_D = contract.Cena_D;
                _contract.SaveChanges();

                return Ok("Contract Update in DB");
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        public async Task<ActionResult<IEnumerable<Contract>>> Delite(int id)
        {
            try
            {
                var rezID = _contract.Contracts.Find(id);
                if (rezID == null)
                {
                    return NotFound($"Contact is not found {id} witch ID");
                }
                _contract.Remove(rezID);
                await _contract.SaveChangesAsync();
                return Ok($"Type  Client witch {id} ID");
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
    }
}

