﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PharmaceuticalCompany.Data;
using PharmaceuticalCompany.Model;

namespace PharmaceuticalCompany.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly AppDbContext _employee;

        public EmployeeController(AppDbContext employee)
        {
            _employee = employee;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Employee>>> GetEmployee()
        {
            try
            {
                return await _employee.Employees.ToListAsync();
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<Employee>>> GetEmployeeId(int id)
        {
            try
            {
                var emp = await _employee.Employees.FindAsync(id);
                if (emp == null)
                {
                    return BadRequest($"Employee is not {id} find ID");
                }
                return Ok(emp);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }

        }
        // POST
        [HttpPost]
        public async Task<ActionResult<IEnumerable<Employee>>> Post(Employee employee)
        {
            try
            {
                _employee.Employees.Add(employee);
                await _employee.SaveChangesAsync();
                return Ok(employee);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        [HttpPut]
        public ActionResult<IEnumerable<Employee>>PutEmpl(Employee employee)
        {

            if (employee == null || employee.EmployeeId == 0)
            {
                if (employee == null)
                {
                    return BadRequest("Employee invalid");
                }
                else if (employee.EmployeeId == 0)
                {
                    return BadRequest($"Employee is not find {employee.EmployeeId} on ID");
                }
            }
            try
            {
                var emp = _employee.Employees.Find(employee.EmployeeId);
                if (emp == null)
                {
                    return NotFound($"Employee is not find  witch id {employee.EmployeeId}");
                }
                emp.Name_E = employee.Name_E;
                emp.Post_E = employee.Post_E;
                emp.Adress_E = employee.Adress_E;
                emp.Pay_E = employee.Pay_E;
                _employee.SaveChanges();
                return Ok("Employee Update");
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }

        }
        [HttpDelete]
        public async Task<ActionResult<IEnumerable<Employee>>> DelEmpl(int id)
        {
            try
            {
                var emplo = await _employee.Employees.FindAsync(id);
                if (emplo == null)
                {
                    return NotFound($"Employee is not found {id} witch ID");
                }
                _employee.Employees.Remove(emplo);
                _employee.SaveChanges();
                return Ok(emplo);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
    }
}

