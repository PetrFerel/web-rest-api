﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PharmaceuticalCompany.Data;
using PharmaceuticalCompany.Model;

namespace PharmaceuticalCompany.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly AppDbContext _client;

        public ClientController(AppDbContext client)
        {
            _client = client;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Client>>> GetClient()
        {
            try
            {
                return await _client.Clients.ToListAsync();
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<Client>>> GetClient(int id)
        {
            try
            {
                var cl = _client.Clients.FindAsync(id);
                if (cl == null)
                {
                    return BadRequest($"Client is not found {id} witch ID");
                }
                return Ok(cl);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        [HttpPut("id")]
        public async Task<ActionResult<IEnumerable<Client>>>PutClient(Client client) // Обновление данных  по id
        {
            if (client != null || client.ClientId == 0)
            {
                if (client == null)
                {
                    return BadRequest("Client invalid");
                }
                else if (client.ClientId == 0)
                {
                    return BadRequest($"Client Id:{client.ClientId} is invalid");
                }
            }
            try
            {
                var cl =  _client.Clients.Find(client.ClientId);
                if (cl == null)
                {
                    return NotFound($"Client not found witch Id:{client.ClientId}");
                }
              
               cl.ClientId = client.ClientId;
                cl.Name_K = client.Name_K;
                cl.Sity_K = client.Sity_K;  
                cl.Adress_K = client.Adress_K;  
                cl.Passwords_K = client.Passwords_K;    
                cl.INN_K = client.INN_K;    
                _client.SaveChanges();
                return Ok("Client Updade");  
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        public async Task<ActionResult<IEnumerable<Client>>> PostClient(Client client)
        {
            try
            {
                _client.Add(client);
                await _client.SaveChangesAsync();
                return Ok(client);
            }

            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }

        }
        [HttpDelete]
        public async Task<ActionResult<IEnumerable<Client>>> DelClient(int id)
        {
            try
            {
                var cl = _client.Clients.Find(id);
                if (cl == null)
                {
                    return BadRequest($"Client is not Found {id} witch ID");
                }
                _client.Clients.Remove(cl);
                await _client.SaveChangesAsync();
                return Ok("Client delite");
            }
            catch (Exception ex)
            {

                return BadRequest(ex);
            }
        }

    }


}