﻿using Microsoft.EntityFrameworkCore;
using PharmaceuticalCompany.Model;

namespace PharmaceuticalCompany.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Client> Clients { get; set; }  
        public DbSet<Employee> Employees { get; set; }  
        public DbSet<Drugs> Drugses { get; set; } 
        public DbSet<TypeDrugs> Type_Drugses { get; set;}
        public DbSet<Contract> Contracts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server = (Localdb)\\mssqllocaldb; database = PharmCompanyDb; Trusted_Connection = true;");
        }
    }
}
