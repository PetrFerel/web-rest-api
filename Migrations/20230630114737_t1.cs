﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PharmaceuticalCompany.Migrations
{
    /// <inheritdoc />
    public partial class t1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    ClientId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name_K = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Sity_K = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Adress_K = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Passwords_K = table.Column<int>(type: "int", nullable: false),
                    INN_K = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.ClientId);
                });

            migrationBuilder.CreateTable(
                name: "Drugses",
                columns: table => new
                {
                    DrugsId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name_D = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Date_VIP_D = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Srok_Godn = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drugses", x => x.DrugsId);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    EmployeeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name_E = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Post_E = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Adress_E = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Pay_E = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.EmployeeId);
                });

            migrationBuilder.CreateTable(
                name: "Type_Drugses",
                columns: table => new
                {
                    TypeDrugsId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type_D = table.Column<string>(type: "nvarchar(1)", nullable: false),
                    Name_D = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Type_Drugses", x => x.TypeDrugsId);
                });

            migrationBuilder.CreateTable(
                name: "Contacts",
                columns: table => new
                {
                    ContactId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ID_D = table.Column<int>(type: "int", nullable: false),
                    ID_E = table.Column<int>(type: "int", nullable: false),
                    ClientId = table.Column<int>(type: "int", nullable: false),
                    Data_C = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Kolvo_D = table.Column<int>(type: "int", nullable: false),
                    Cena_D = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacts", x => x.ContactId);
                    table.ForeignKey(
                        name: "FK_Contacts_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "ClientId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Contacts_ClientId",
                table: "Contacts",
                column: "ClientId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contacts");

            migrationBuilder.DropTable(
                name: "Drugses");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Type_Drugses");

            migrationBuilder.DropTable(
                name: "Clients");
        }
    }
}
