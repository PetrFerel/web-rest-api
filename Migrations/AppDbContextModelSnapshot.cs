﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using PharmaceuticalCompany.Data;

#nullable disable

namespace PharmaceuticalCompany.Migrations
{
    [DbContext(typeof(AppDbContext))]
    partial class AppDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.8")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("PharmaceuticalCompany.Model.Client", b =>
                {
                    b.Property<int>("ClientId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("ClientId"));

                    b.Property<string>("Adress_K")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("INN_K")
                        .HasColumnType("int");

                    b.Property<string>("Name_K")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Passwords_K")
                        .HasColumnType("int");

                    b.Property<string>("Sity_K")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ClientId");

                    b.ToTable("Clients");
                });

            modelBuilder.Entity("PharmaceuticalCompany.Model.Contact", b =>
                {
                    b.Property<int>("ContactId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("ContactId"));

                    b.Property<int>("Cena_D")
                        .HasColumnType("int");

                    b.Property<int>("ClientId")
                        .HasColumnType("int");

                    b.Property<DateTime>("Data_C")
                        .HasColumnType("datetime2");

                    b.Property<int>("ID_D")
                        .HasColumnType("int");

                    b.Property<int>("ID_E")
                        .HasColumnType("int");

                    b.Property<int>("Kolvo_D")
                        .HasColumnType("int");

                    b.HasKey("ContactId");

                    b.HasIndex("ClientId");

                    b.ToTable("Contacts");
                });

            modelBuilder.Entity("PharmaceuticalCompany.Model.Drugs", b =>
                {
                    b.Property<int>("DrugsId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("DrugsId"));

                    b.Property<DateTime>("Date_VIP_D")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name_D")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Srok_Godn")
                        .HasColumnType("int");

                    b.HasKey("DrugsId");

                    b.ToTable("Drugses");
                });

            modelBuilder.Entity("PharmaceuticalCompany.Model.Employee", b =>
                {
                    b.Property<int>("EmployeeId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("EmployeeId"));

                    b.Property<string>("Adress_E")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name_E")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<float>("Pay_E")
                        .HasColumnType("real");

                    b.Property<string>("Post_E")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("EmployeeId");

                    b.ToTable("Employees");
                });

            modelBuilder.Entity("PharmaceuticalCompany.Model.TypeDrugs", b =>
                {
                    b.Property<int>("TypeDrugsId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("TypeDrugsId"));

                    b.Property<string>("Name_D")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Type_D")
                        .IsRequired()
                        .HasColumnType("nvarchar(1)");

                    b.HasKey("TypeDrugsId");

                    b.ToTable("Type_Drugses");
                });

            modelBuilder.Entity("PharmaceuticalCompany.Model.Contact", b =>
                {
                    b.HasOne("PharmaceuticalCompany.Model.Client", "Clients")
                        .WithMany()
                        .HasForeignKey("ClientId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Clients");
                });
#pragma warning restore 612, 618
        }
    }
}
