﻿using System.ComponentModel.DataAnnotations;

namespace PharmaceuticalCompany.Model
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        [Required]
        public string Name_E { get; set; }
        [Required]  
        public string Post_E { get; set; } // Должность
        [Required]
        public string Adress_E { get; set; }
        [Required]
        public float Pay_E { get; set; }
    }
}
