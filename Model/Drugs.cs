﻿using System.ComponentModel.DataAnnotations;

namespace PharmaceuticalCompany.Model
{
    public class Drugs
    {
        public int DrugsId { get; set; }
        [Required]
        public string Name_D { get; set; }
        [Required]  
        public DateTime Date_VIP_D { get; set; }
        [Required]  
        public int Srok_Godn { get; set; }

    }
}
