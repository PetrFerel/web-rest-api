﻿using System.ComponentModel.DataAnnotations;

namespace PharmaceuticalCompany.Model
{
    public class Client
    {
       
        public int ClientId { get; set; }
        [Required]
        public string Name_K { get; set; }
        [Required]  
        public string Sity_K { get; set;}
        [Required]  
        public string Adress_K { get; set; }
        [Required]  
        public int Passwords_K { get; set; }
        [Required]  
        public int INN_K { get; set; }
    }
}
