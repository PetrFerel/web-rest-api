﻿using System.ComponentModel.DataAnnotations;

namespace PharmaceuticalCompany.Model
{
    public class Contract
    {
        public int ContractId { get; set; }
        [Required]
        public int ID_C { get; set; }
        [Required]
        public int ID_D { get; set;}
        [Required]
        public int ID_K { get; set;}
        public int ID_E { get; set; }
        [Required]
        public int ClientId { get; set; }
        [Required]
        public DateTime Data_C { get; set; }
    
        public int Kolvo_D { get; set; }
        [Required]
        public int Cena_D { get; set; }
        [Required]
        public virtual Client Clients { get; set; }

    }
}
