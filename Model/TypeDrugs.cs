﻿using System.ComponentModel.DataAnnotations;

namespace PharmaceuticalCompany.Model
{
    public class TypeDrugs
    {
        public int TypeDrugsId { get; set; }
        [Required]
        public string Type_D { get; set; }
        [Required]  
        public string Name_D { get; set; }
    }
}
